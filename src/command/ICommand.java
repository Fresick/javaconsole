package command;

public interface ICommand {

	public CommandReturn call(CallerInfo info, CommandParam param);
	public Class<?> getParamType();
	
}
