package command;

import java.util.HashMap;

public class CommandRegistry {

	HashMap<String, ICommand> map = new HashMap<>();
	
	public void register(String commString, ICommand command){
		map.put(commString, command);
	}

	public ICommand getCommand(String commString){
		return map.get(commString);
	}
	
}
