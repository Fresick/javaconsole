package command;

public abstract class CallerInfo {

	protected ICaller caller;
	
	public CallerInfo(ICaller caller) {
		this.caller = caller;
	}
	
	public ICaller getCaller(){
		return caller;
	}
	
	public abstract String getName();
	
}
