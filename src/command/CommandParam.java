package command;

public abstract class CommandParam {

	protected String line;
	
	public CommandParam(String line) {
		this.line = line;
	}
	
	public abstract Object getData();
	public abstract Class<?> getType();
	
}
