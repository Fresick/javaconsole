package command;

public interface ICaller {

	public CallerInfo getInfo();
	public void stop();
	
}
