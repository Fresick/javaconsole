package command;

public abstract class CommandReturn {

	protected CallerInfo info;
	protected CommandParam param;
	
	public CommandReturn(CallerInfo info, CommandParam param) {
		this.info = info;
		this.param = param;
	}
	
	public abstract Class<?> getType();
	public abstract Object getData();
	
}
