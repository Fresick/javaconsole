package console;

import java.util.Scanner;

import command.CommandRegistry;
import command.CommandReturn;
import command.ICaller;
import command.CallerInfo;
import command.ICommand;
import command.CommandParam;

public class Console implements ICaller {

	private boolean stop = false;

	private CommandRegistry reg;
	private Scanner scanner;

	public static void main(String[] args) {
		Console cons = new Console();

		cons.reg = new CommandRegistry();
		cons.scanner = new Scanner(System.in);

		cons.reg.register("echo", new ICommand() {

			@Override
			public CommandReturn call(CallerInfo info, CommandParam param) {
				return new CommandReturn(info, param) {

					@Override
					public Class<?> getType() {
						return String.class;
					}

					@Override
					public Object getData() {
						return this.param.getData().toString();
					}
				};
			}

			@Override
			public Class<?> getParamType() {
				return String.class;
			}
		});

		cons.reg.register("stop", new ICommand() {

			@Override
			public CommandReturn call(CallerInfo info, CommandParam param) {
				info.getCaller().stop();
				return new CommandReturn(info, param) {

					@Override
					public Class<?> getType() {
						return String.class;
					}

					@Override
					public Object getData() {
						return "Stopping!";
					}
				};
			}

			@Override
			public Class<?> getParamType() {
				return null;
			}
		});

		String line;
		while (!cons.stop) {
			line = cons.scanner.nextLine();
			int firstSpace = line.indexOf(' ');
			if (firstSpace == -1)
				firstSpace = line.length();
			String commandStr = line.substring(0, firstSpace);
			ICommand command = cons.reg.getCommand(commandStr);
			CommandReturn ret = null;
			if (command != null)
				ret = command.call(cons.getInfo(), cons.getParams(line, command));
			if (ret != null) {
				if (ret.getType() == String.class)
					System.out.println((String) (ret.getData()));
			}
		}
	}

	private CommandParam getParams(String line, ICommand command) {
		if (command.getParamType() == null)
			return null;
		CommandParam param = new CommandParam(line) {

			@Override
			public Class<?> getType() {
				return command.getParamType();
			}

			@Override
			public Object getData() {
				if (command.getParamType() == String.class) {
					int firstSpace = line.indexOf(' ');
					if (firstSpace <= 0)
						return "";
					return line.substring(firstSpace + 1).trim();
				}
				return null;
			}
		};
		return param;
	}

	@Override
	public void stop() {
		this.stop = true;
	}

	@Override
	public CallerInfo getInfo() {
		return new CallerInfo(this) {

			@Override
			public String getName() {
				return "console";
			}
		};
	}

}
